#include <stdio.h>
#include <string.h>
int main() 
{
   char s[100];
   int i=0;
   printf("\nEnter a string : ");
   gets(s);  
   while (s[i]!='\0')
   {
      if(s[i] >= 'A' && s[i] <= 'Z')   
      s[i] = s[i]+32;   
      else
      s[i] = s[i]-32;
      i++;
   }
   s[i]='\0';
   printf("\nString in Upper Or Lower Case = %s", s);
   return 0;
}
