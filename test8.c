#include <stdio.h>
struct employee
{
    char name[30];
    int empId;
    float salary;
}; 
int main()
{
    struct employee emp;
    printf("\nEnter details :\n");
    printf("Name of the employee?:");          
    gets(emp.name);
    printf("ID of the employee?:");            
    scanf("%d",&emp.empId);
    printf("Salary of the employee?:");        
    scanf("%f",&emp.salary);
    printf("\n***Entered detail is:***\n");
    printf("Name: %s\n"   ,emp.name);
    printf("Id: %d\n"     ,emp.empId);
    printf("Salary: %f\n",emp.salary);
    return 0;
}
